# User original docker python 3.8.0 image
FROM python:3.8.0-slim

# Add script to the / dir
ADD ./app.py /app.py

# Set the script as the entrypoint
ENTRYPOINT ["python", "/app.py"]