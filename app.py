import sys
import re

from itertools import groupby
from itertools import combinations_with_replacement


def read_stdin_data():
    """
    Read the data from stdin
    We going to use native sys.stdin
    to access the unix system stdin stream
    instead of "input()" due to intuitice reasons

    Returns:
        The whole utf-8 encoded text from stdin stream
    """
    return sys.stdin.read()


def parse_data(data):
    """
    Due to example in "requirements.pdf"
    We expect that the input consists of two parts:
    The first part with the bouquet designs
    The second is the flowers
    The two parts are separaed by the "\n" char
    If there less or more parts then two then we
    recognize that behaviour as error and raise it then

    Args:
        data: The string contains the std input data
    """
    splited_data = stdin_data.split('\n\n')

    if len(splited_data) != 2:
        raise ValueError('Wrong stdin data format')

    return splited_data


def parse_flower(flower_string):
    """
    Parse the flower string
    Convert the flower sting into the tuple of (specie, size)
    Validate both flower specie and size
    If valiedation fails raise ValueError

    Args:
        flower_string: The string with the flower string(aS)

    Returns:
        The tuple with (specie, size) of the flower
    """
    if len(flower_string) != 2:
        raise ValueError('The flower is incorect')

    specie, size = flower_string

    if not specie.islower() or not specie.isalpha():
        raise ValueError('The flower specie must be both lowercase and alpha')

    if not size.isupper() or size not in 'SL':
        raise ValueError('The flower size must be both '
                         'uppercase and only have value S or L')

    return specie, size


def parse_bouquet_total_quantity(bouquet_design_string):
    """
    Try to receive the bouquet total quantity from the
    bouquet design string if this operation fails then
    throws the error

    Args:
        bouquet_design_string: String with the bouquet design

    Returns:
        The int value representing the total flowers quantity
        in the given bouquet per given bouquet design
    """
    bouquet_design_total_quantity_regex = r'([0-9]+)$'

    total_quantity = re.search(bouquet_design_total_quantity_regex,
                               bouquet_design_string).group()

    if not total_quantity:
        raise ValueError('The total quantity is not present in design')

    return int(total_quantity)


def parse_bouquet_flower_groups(bouquet_design_string):
    """
    Try to receive the bouquet flower groups from the
    bouquet design string if this operation fails then
    throws the error

    Args:
        bouquet_design_string: String with the bouquet design

    Returns:
        The list of tuples the first tuple item is the
        flower specie the second is the flower quantity
    """
    flower_group_regex = r'([0-9]*[a-z])'
    flower_group_string_list = re.findall(flower_group_regex,
                                          bouquet_design_string[2:])

    if not len(flower_group_string_list) > 0:
        raise ValueError('The flower groups are not present in bouquet design')

    return list(map(lambda x: (x[-1], int(x[:-1])), flower_group_string_list))


def parse_bouquet_name(bouquet_design_string):
    """
    Try to receive the bouquet name from the
    bouquet design string if this operation fails then
    throws the error

    Args:
        bouquet_design_string: String with the bouquet design

    Returns:
        The bouquet name
    """
    name = bouquet_design_string[0]

    if not name.isupper() or not name.isalpha():
        raise ValueError('The bouquet name must be both uppercase and alpha')

    return name


def parse_bouquet_size(bouquet_design_string):
    """
    Try to receive the bouquet size(S/L)from the
    bouquet design string if this operation fails then
    throws the error

    Args:
        bouquet_design_string: String with the bouquet design

    Returns:
        The string bouquet size (L/S)
    """
    name = bouquet_design_string[1]

    if not name.isupper() or name not in 'SL':
        raise ValueError('The bouquet size must be both '
                         'uppercase and only have value S or L')

    return name


def parse_bouquet_design(bouquet_design_string):
    """
    Split the bouquet design string by elements
    such as bouquet name, bouquet size, flower_groups,
    bouquet_design_total_quantity

    If somewere parsing fails raises the error

    Args:
        bouquet_design_string: String represent the bouquet design

    Returns:
        Bunch of params as bouquet name, bouquet size, flower_groups,
        bouquet_design_total_quantity
    """
    if len(bouquet_design_string) <= 2:
        raise ValueError('The boquet deisgn is too small')

    name = parse_bouquet_name(bouquet_design_string)
    size = parse_bouquet_size(bouquet_design_string)
    flower_groups = parse_bouquet_flower_groups(bouquet_design_string)
    total_quantity = parse_bouquet_total_quantity(bouquet_design_string)

    return name, size, flower_groups, total_quantity


def aggregate_flowers(flower_string):
    """
    Convert the string with flower into the aggregation dict
    or append the flower to the existing aggregation

    The flow is the next: flowers -> groupby size(L/S)
    -> groupBy specie(a-z).

    Args:
        flower_string: The string with the flower string(aS)
        aggreagation: The aggregation dictionary which
                      contain all described abowe

    Returns:
        The updated aggregation dictionary
        which contain all described abowe
    """

    aggregation = {}
    flower_strings = flower_string.split('\n')
    flowers = sorted([parse_flower(flower_string) for flower_string
                      in flower_strings],
                     key=lambda x: x[1])
    flowers_aggregation_by_size = groupby(flowers, lambda x: x[1])

    for size, size_aggregation in flowers_aggregation_by_size:
        if not aggregation.get(size):
            aggregation[size] = {}

        size_aggregation = sorted(size_aggregation, key=lambda x: x[0])
        flowers_aggregation_by_specie = groupby(size_aggregation,
                                                key=lambda x: x[0])

        for specie, specie_aggregation in flowers_aggregation_by_specie:
            aggregation.get(size)[specie] = len(list(specie_aggregation))

    return aggregation


def aggregate_bouquet_design(bouquet_design_string):
    """
    Convert the string with bouquets into the aggregation dict
    Apply grouping for the designs

    The flow is the next: bouquet_designs -> groupby size(L/S)
    -> groupBy name(A-Z).

    Then we convert the sequense of flowers present in design
    into the key:value dict where key is the flower specie
    and the value is the flower quantity for current design

    For future lets call combination of flower specie +
    flower quantity as flower group

    Args:
        bouquet_designs_string: The string with bouquet designs
                               separated by "\n"

    Returns:
        The aggregation dictionary which contain all described abowe
    """
    name, size, flower_groups, total_quantity =\
        parse_bouquet_design(bouquet_design_string)

    aggregation = {
       'name': name,
       'size': size,
       'total_quantity': total_quantity,
       'flower_groups': {}
    }

    for flower_specie, flower_quantity in flower_groups:
        aggregation['flower_groups'][flower_specie] = flower_quantity

    return aggregation


def bouquet_groups_to_bouquet_string(bouquet_name,
                                     bouquet_size,
                                     bouquet_groups):
    """
    Create the string from the related to the bouquet format:

    <bouquet name><bouquet size><flower 1 quantity><flower 1
    specie>...<flower N quantity><flower N specie>

    Args:
        bouquet_name: The name of the bouquet.
                     Is indicated by a single, uppercase letter: A - Z
        bouquet_size: The size of the bouquet.
                     Is indicated by a single,
                     uppercase letter: L (large) and S (small).
        bouquet_groups: The groups of the bouquet
                       as the dict where the keys
                       are the flower species and
                       the values is the quantity of them

    Returns:
        The string due to bouquet described in the pdf and mantioned above.
    """
    sorted_groups = sorted(bouquet_groups.items(), key=lambda x: x[0])
    sorted_groups_string = ''.join(map(lambda x: f'{x[1]}{x[0]}',
                                   sorted_groups))

    return f'{bouquet_name}{bouquet_size}{sorted_groups_string}'


def parse_bouquet_design_aggregation(bouquet_design_aggregation):
    """
    The trivial helper function
    Parse necessary dict keys to increase the readability of main code
    Remove sum getter masssy stuff

    Args:
        bouquet_design_aggregation: The dict that aggregate all necessary
                                   info about the given bouquet design
                                   Make easy to retrieve that info
    Returns:
        The necassary and usefull data from that dict:
            bouquet_name: The name of the given bouquet (A-Z)
            bouquet_size: The size of the bouquet (S/L)
            bouquet_flower_species: The flower species
                                   unique per bouquet (a-z)
            bouquet_flower_groups: The groups of the bouquet
                                  as the dict where the keys
                                  are the flower species
                                  and the values is
                                  the quantity of them ({'a':12, 'b':2}) ...
            bouquet_total_quantity: The total quantity of flowers
                                   that can be in the bouquet
                                   by that bouquet design
    """
    bouquet_flower_groups = bouquet_design_aggregation.get('flower_groups')
    bouquet_total_quantity = bouquet_design_aggregation.get('total_quantity')
    bouquet_size = bouquet_design_aggregation.get('size')
    bouquet_name = bouquet_design_aggregation.get('name')
    bouquet_flower_species = set(bouquet_design_aggregation
                                 .get('flower_groups').keys())

    return bouquet_name, bouquet_size, bouquet_flower_species,\
        bouquet_flower_groups, bouquet_total_quantity


def parse_flower_aggregation(flowers_aggregation, bouquet_size):
    """
    The trivial helper function
    Parse necessary dict keys to increase the readability of main code
    Remove sum getter messy stuff

    Args:
        flowers_aggregation: The dict that aggregate all necessary
                             info about the all available flowers
                             that we will use to craft the bouquet
                             Make easy to retrieve that info
        bouquet_size: The size of the bouquet (S/L)

    Returns:
        The necessary and useful data from that dict:
            flower_species: The unique set of flower species
                            we can use. ({'a', 'b' ..., 'c'})
            flower_groups: The groups from available flowers
                           as the dict where the keys
                           are the flower species
                           and the values is
                           the quantity of them ({'a':12, 'b':2}) ...
            flower_total_quantity: The total available quantity
                                   of flowers.
    """
    flower_groups = flowers_aggregation.get(bouquet_size)
    flower_total_quantity = sum(flower_groups.values())
    flower_species = set(flowers_aggregation.get(bouquet_size).keys())

    return flower_species, flower_groups, flower_total_quantity


def is_bouquet_can_be_craft(flower_total_quantity,
                            bouquet_total_quantity,
                            flower_species,
                            bouquet_flower_species,
                            compare_flower_quantity):
    """
    The helper function to encapsulate the validation logic.
    It checks such stuff as:
        - Is flowers enough to create the bouquet for given design
        - Is flowers enough for each specie to create the bouquet
          for given bouquet design
        - Is flower species variability enough to implement the
          given bouquet design

    Args:
        flower_total_quantity: The total quantity of flowers we have
                               to create the  bouquets
        bouquet_total_quantity: The total allowed quantity of flowers
                               to have in the bouquet by design
        flower_species: The set with total mount of flower species
                        we have to create the bouquet
        bouquet_flower_species: The minimal set of flower species
                               we must to have in each bouquet due
                               to the provided design
        compare_flower_quantity: Closure function to compare the quantity
                                 for each specie of available flowers and
                                 the species required by the bouquet design

    Returns:
        The bool value that show if we can create the bouquet from the
        flowers we have.
    """
    total_quantity_enough = flower_total_quantity >= bouquet_total_quantity
    flower_species_enough = flower_species >= bouquet_flower_species
    flower_species_quantity_enough = all(compare_flower_quantity(specie)
                                         for specie in bouquet_flower_species)
    return total_quantity_enough\
        and flower_species_enough\
        and flower_species_quantity_enough


def craft_available_bouquets(flowers_aggregation, bouquet_design_aggregation):
    """
    For given bouquet design aggregation craft all possible combinations
    to fit the total quantity of bouquet design.

    Args:
        flowers_aggregation: Flower aggregation dictionary
        bouquet_design_aggregation: bouquet aggregation dictionary

    Returns:
        The list of bouquets that can be created on given aggregations
    """
    bouquet_groups = []

    # Parse bouquet all main info
    bouquet_name, bouquet_size, bouquet_flower_species,\
        bouquet_flower_groups, bouquet_total_quantity =\
        parse_bouquet_design_aggregation(bouquet_design_aggregation)

    # Make the same for flowers info
    flower_species, flower_groups, flower_total_quantity =\
        parse_flower_aggregation(flowers_aggregation, bouquet_size)

    # Define closure function to compare flower quantity for each specie
    # in both flowers present and flowers in bouquet design
    def compare_flower_quantity(specie):
        nonlocal flower_groups
        nonlocal bouquet_flower_groups

        q1 = flower_groups.get(specie)
        q2 = bouquet_flower_groups.get(specie)

        return q1 >= q2

    # Check if we can craft any bouquet for the provided design
    bouquet_can_be_crafted = is_bouquet_can_be_craft(flower_total_quantity,
                                                     bouquet_total_quantity,
                                                     flower_species,
                                                     bouquet_flower_species,
                                                     compare_flower_quantity)

    if bouquet_can_be_crafted:
        # Calc if we have free space for flowers in our bouquet
        # by the given design
        extra_bouquet_space = bouquet_total_quantity -\
            sum(bouquet_flower_groups.values())

        if extra_bouquet_space:
            # Get the species that was not use in bouquet design
            extra_species = flower_species - bouquet_flower_species

            # Calc all possible combinations of flowers available
            combinations =\
                list(combinations_with_replacement(extra_species,
                                                   extra_bouquet_space))

            # Construct the flower group for each combination
            for combination in combinations:
                extra_bouquet_flower_groups = {}
                extra_bouquet_aggregation_by_specie = groupby(combination)

                for specie, specie_aggregation\
                        in extra_bouquet_aggregation_by_specie:
                    extra_bouquet_flower_groups[specie] =\
                        len(list(specie_aggregation))

                bouquet_groups.append({**bouquet_flower_groups,
                                      **extra_bouquet_flower_groups})

        else:
            bouquet_groups.append(bouquet_flower_groups)

    return [bouquet_groups_to_bouquet_string(bouquet_name, bouquet_size, g)
            for g in bouquet_groups]


def handle_flower_stream(flowers_string, bouquet_designs_string):
    """
    Generator

    Iterate over flowers and return all bouquets that
    can be created by bouquet design

    Args:
        bouquet_designs_string: The string with bouquet designs
                               separated by "\n"
        flowers_string: The string with the flower string(aS)

    Returns:
        All bouquets from collected flowers
    """
    bouquet_design_strings = bouquet_designs_string.split('\n')
    flowers_aggregation = aggregate_flowers(flowers_string)

    for bouquet_design_string in bouquet_design_strings:
        bouquet_design_aggregation =\
            aggregate_bouquet_design(bouquet_design_string)

        bouquets = craft_available_bouquets(flowers_aggregation,
                                            bouquet_design_aggregation)

        for bouquet in bouquets:
            yield bouquet


if __name__ == '__main__':
    try:
        # Read data from stdin
        stdin_data = read_stdin_data().strip()
        # Parse the stdin data for two parts
        bouquet_designs_string, flowers_string = parse_data(stdin_data)

        handler = handle_flower_stream(flowers_string, bouquet_designs_string)

        for bouquet in handler:
            sys.stdout.write(f'{bouquet}\n')

    except KeyboardInterrupt as e:
        sys.stdout.write('Exited\n')
