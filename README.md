# Code challenge
Main application has been writing on Python programming language
The python versionmust be upper then python v3.7.0

### File structure
- app.py
- build.sh
- run.sh
- sample.txt
- README.md
- Dockerfile

**app.py** - The code of main application.  
**build.sh** - The code to create docker image for challenge.  
**run.sh** - The code to run the application in the docker.  
**sample.txt** - The input test data for the program  
**README.md** - Code details and info.  
**Dockerfile** The file to build the docker image for this challenge.  

### Instructions
Firstly build the image
```
$ ./build.sh
```
Now you can test the application
To use it simply send data to the stdin the following way:
```
$ cat sample.txt | ./run.sh
```
For the sample.txt the result will be the following:
```
$ cat sample.txt | ./run.sh
AL10a15b5c
AS10a10b5c
BL5a15b1c
BS1a10b5c
CL20a10b15c
DL20b8c
DL1a20b7c
DL2a20b6c
DL3a20b5c
DL4a20b4c
DL5a20b3c
DL6a20b2c
DL7a20b1c
DL8a20b
```
### Ps
* All code details are explain in the code hints.
* Some functions are overhinted to make it easier to read
* Some code function are overdecomposed to make code more readable